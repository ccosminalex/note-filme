﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace noteFilme
{
    class Program
    {
        static void Main(string[] args)
        {
            int nrFilme;
            Console.Write("Introduceti nr de filme: ");
            while(int.TryParse(Console.ReadLine(), out nrFilme) == false || nrFilme < 1)
            {
                Console.WriteLine("Nr invalid. Introduceti din nou:");
            }
            string[] filme = new string[nrFilme];
            if(nrFilme == 1)
                Console.WriteLine("Introduceti filmul:");
            else
                Console.WriteLine("Introduceti filmele:");
            for(int i = 0; i < nrFilme; i++)
            {
                filme[i] = Console.ReadLine();
            }
            int[][] note = new int[3][];
            for(int i = 0; i < filme.Length; i++)
            {
                Console.WriteLine("Dati note filmului " + filme[i]);
                string input = "";
                note[i] = new int[0]; 
                bool next = false;
                while(!next)
                { 
                    input = Console.ReadLine();
                    if (input == "next")
                        next = true;
                    while ((int.TryParse(input, out int nota) == false || nota < 1 || nota > 10) && !next)
                    {
                        Console.WriteLine("Nota invalida. Introduceti din nou:");
                        input = Console.ReadLine();
                        if (input == "next")
                            next = true;
                    }
                    if (input != "next")
                    {
                        Array.Resize(ref note[i], note[i].Length + 1);
                        note[i][note[i].Length - 1] = int.Parse(input);
                    }
                }
                
            }
            float media;
            float suma;
            for (int i = 0; i < filme.Length; i++)
            {
                media = 0;
                suma = 0;
                if(note[i].Length == 0)
                    Console.Write("Filmul " + filme[i] + " nu are note");
                else
                    if(note[i].Length == 1)
                    Console.Write("Filmul " + filme[i] + " are nota: ");
                else
                    if(note[i].Length > 1)
                        Console.Write("Filmul " + filme[i] + " are notele: ");
                for (int j = 0; j < note[i].Length; j++)
                {
                    Console.Write(note[i][j]);
                    if (j < note[i].Length - 1)
                        Console.Write(", ");
                    suma += note[i][j];
                }
                if(suma > 0)
                    media = (float)suma / note[i].Length;
                Console.WriteLine(" si un rating in valoare de " + media + ".");
            }
            Console.ReadKey();
        }
    }
}
